Usage
=====

To use the Strapi Profairs Synchroniser, 
run
```sh
cd repo_location/strapi_profairs_sync && poerty run python main.py
```

or set a cronjob.

To run the `specific_update` script to enable the companies to see their changes on the website
as soon as they make changes to Profairs 
run 

```sh
cd repo_location/strapi_profairs_sync && poerty run python specific_update.py
```