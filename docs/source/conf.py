# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import sphinx_rtd_theme

# Path to the directory containing your Python modules
sys.path.insert(0, os.path.abspath('../../strapi_profairs_sync'))



# -- Project information -----------------------------------------------------

project = 'Strapi Profairs Synchroniser'
copyright = '2023, Abdullah Ahmed'
author = 'Abdullah Ahmed'

# The full version, including alpha/beta/rc tags
release = '2.5.0'


# -- General configuration ---------------------------------------------------
# Document all modules in the strapi_profairs_sync package
autodoc_default_flags = ['members']
autodoc_member_order = 'bysource'
autodoc_mock_imports = [
    'asyncio',
    'aiohttp',
    'requests',
    'gevent',
    'grequests',
    'httpx',
    'rapidjson',
    'python_rapidjson',
    'tqdm',
    'dotenv',
    'urllib3'
]
autodoc_docstring_signature = True
# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']