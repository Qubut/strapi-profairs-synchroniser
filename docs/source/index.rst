.. Strapi Profairs Synchroniser documentation master file, created by
   sphinx-quickstart on Wed Mar 15 11:46:30 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Strapi Profairs Synchroniser's documentation!
========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`