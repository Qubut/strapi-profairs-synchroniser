Methods
========

.. toctree::
   :maxdepth: 3

   create_methods
   update_methods
   delete_methods