"""
This script updates or creates records in the Strapi CMS based on data obtained from Profairs.

It uses the aiohttp library to make asynchronous HTTP requests to the Strapi CMS API.

It also uses functions from the update_methods, create_methods, and delete_methods modules to update, create,
and delete Strapi records.

The script first retrieves data from Profairs using the get_profairs_data function, and then retrieves Strapi 
records using the get_strapi_ausstellers function.

For each aussteller in the Profairs data, the script checks if a corresponding Strapi record exists. If it exists,
the script updates the Strapi record using the update_aussteller, update_ansprechpartner, and update_onlinemedien
functions. If it doesn't exist, the script creates a new Strapi record using the create_aussteller, create_ansprechpartner,
and create_onlinemedien functions.

The script also deletes Strapi records that don't have corresponding aussteller data in the Profairs data, as well as 
unpublished Strapi records that have the same ausstellerid as the ones in the Profairs data.

To run the script, simply execute the script directly from the command line or import it into another module and call the main function.

"""


import os
from aiohttp import ClientSession
from utils.utility import get_profairs_data, get_strapi_ausstellers
from logger import logger
from typing import Coroutine, Any, Dict
from methods.update_methods import (
    update_aussteller,
    update_onlinemedien,
    update_ansprechpartner,
)
from methods.create_methods import (
    create_aussteller,
    create_ansprechpartner,
    create_onlinemedien,
    create_stellenausschreibungen,
)
from methods.delete_methods import delete
import asyncio


# Change the working directory to the directory containing this script
os.chdir(os.path.dirname(os.path.abspath(__file__)))


async def retry_coroutine(
    coroutine: Coroutine[Any, Any, Any],
) -> Any | None:
    retries = 3  # number of retries before giving up
    retry_delay = 1  # delay between retries in seconds
    while retries > 0:
        try:
            result = await coroutine
            return result if result else None
        except:
            retries -= 1
            if retries < 0:
                logger.error(f'failed to update {result.get("firma","Data")}')
            else:
                # wait before retrying
                await asyncio.sleep(retry_delay)
                continue


async def main():
    """
    The main function that updates or creates records in the Strapi CMS based on data obtained from Profairs.
    """
    logger.info("Started")
    # Get data from Profairs and Strapi
    data, unpublished_ausstellers, strapi_ausstellers = await asyncio.gather(
        get_profairs_data(),
        get_strapi_ausstellers(published=False),
        get_strapi_ausstellers(),
    )
    async with ClientSession() as session:
        ids: Dict[int, Dict[str, Any]] = (
            {
                int(au["ausstellerid"]): au
                for au in strapi_ausstellers
                if au["ausstellerid"]
            }
            if len(strapi_ausstellers) > 0
            else dict()
        )
        unpublished_ids: Dict[int, Dict[str, Any]] = (
            {
                int(au["ausstellerid"]): au
                for au in unpublished_ausstellers
                if au["ausstellerid"]
            }
            if (uas := unpublished_ausstellers) and len(uas) > 0
            else dict()
        )
        for aussteller in data:
            # IF aussteller is published in Strapi
            if strapi_a := ids.get(aussteller["ausstellerid"], None):
                logger.info(f"found {aussteller['firma']} in Strapi")

                updated_a = await retry_coroutine(
                    update_aussteller(session, strapi_a, aussteller)
                )
                if updated_a:
                    await retry_coroutine(
                        update_ansprechpartner(
                            strapi_a, aussteller, session))
                    await retry_coroutine(update_onlinemedien(strapi_a, aussteller, session)),
                    await retry_coroutine(create_stellenausschreibungen(
                        updated_a.id(),
                        aussteller,
                        updated_a.logo,
                        updated_a.firmenlogo,
                    )
                    )
                else:
                    continue

            else:
                # check if not published
                if unpublished_aussteller := unpublished_ids.get(
                    aussteller["ausstellerid"], None
                ):
                    logger.info(
                        f"found {aussteller['firma']} unpublished in Strapi")
                    await retry_coroutine(
                        create_stellenausschreibungen(
                            unpublished_aussteller.get("ausstellerid"),
                            aussteller,
                            logo["id"]
                            if (logo := unpublished_aussteller.get("logo"))
                            else None,
                            flogo["id"]
                            if (flogo := unpublished_aussteller.get("firmenlogo"))
                            else None,
                        )
                    )

                else:
                    # create new Aussteller in Strapi
                    logger.info(f"Create {aussteller['firma']}")
                    created_a = await retry_coroutine(create_aussteller(aussteller))
                    id = created_a.id()
                    if created_a:
                        await retry_coroutine(
                            asyncio.gather(
                                create_ansprechpartner(id, aussteller),
                                create_onlinemedien(id, aussteller),
                                create_stellenausschreibungen(
                                    id, aussteller, created_a.logo, created_a.firmenlogo
                                ),
                            )
                        )
                    else:
                        continue

    profiars_ids = [int(e["ausstellerid"]) for e in data]
    for a in (
        a for a in strapi_ausstellers if int(a["ausstellerid"]) not in profiars_ids
    ):
        await delete(a)
    for a in (
        a for a in unpublished_ausstellers if int(a["ausstellerid"]) not in profiars_ids
    ):
        await delete(a)

    logger.info("Finished")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
