import os
import json
from enum import Enum


class Config:
    """
    A class that reads configuration settings from a default configuration file and an optional environment-specific
    configuration file, and provides methods for accessing the settings.
    """

    def __init__(self, env=None):
        """
        Initialize the Config object.

        Parameters:
            - env: str, optional. The name of the environment configuration file to read. Defaults to None.
        """
        # Load the default configuration file
        path = os.path.dirname(os.path.realpath(__file__))
        config_json = "/".join([path, "default.json"])

        with open(config_json) as f:
            self.config = json.load(f)

        # If an environment name was provided, read the corresponding environment configuration file
        if env:
            env_config_path = f"config/{env}.json"
            if os.path.exists(env_config_path):
                with open(env_config_path) as f:
                    env_config = json.load(f)
                self.config.update(env_config)

    def get_headers(self):
        """
        Get the headers from the configuration file.

        Returns:
            - dict: a dictionary containing the headers and their values.
        """
        return self.config.get("headers", {})

    def get_studiengänge(self):
        """
        Get the studiengänge from the configuration file.

        Returns:
            - dict: a dictionary containing the studiengänge and their values.
        """
        return self.config.get("studiengänge", {})

    def get_fakultäten(self):
        """
        Get the fakultäten from the configuration file.

        Returns:
            - list: a list of the fakultäten options.
        """
        return self.config.get("fakultäten", {})

    def get_endpoints(self):
        """
        Retrieves endpoints from the configuration file.

        Returns:
        - A dictionary containing endpoint information.
        """
        return self.config.get("endpoints", {})


class MESSESTÄNDE(Enum):
    """
    An enumeration of messestände options.
    """

    A = 1
    B = 2
