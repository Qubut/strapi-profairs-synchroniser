"""
This module provides functions for creating exhibitor data and related entries in a Strapi instance. 
It also provides a function to create a post request to the given endpoint with the given request data.

Functions:
    post(request: Dict[str, Any], endpoint: str) -> int: 
    Post request to the given endpoint with the given request data.
    
    create_aussteller(aussteller: Dict[str, Any]) -> int: 
    Create an exhibitor with the given data.
    
    create_stellenausschreibungen(strapi_aussteller_id, aussteller, logo: str, firmenlogo: str) -> None: 
    Creates job postings for an exhibitor and saves them to a Strapi exhibitor.
"""
import rapidjson as json
from typing import Dict, Any

from aiohttp import ClientSession

from config.endpoints import strapi_url
from config.config import MESSESTÄNDE, Config
from classes.aussteller import Aussteller
from classes.ansprechpartner import Ansprechpartner
from classes.onlinemedien import Onlinemedien
from classes.stellenausschreibung import Stellenausschreibung
from utils.utility import (
    get_studiengänge,
    set_aussteller_data,
    set_onlinemedien_data,
    set_ansprechpartner_data,
)
from logger import logger
from utils.upload_media import upload_multiple_media


async def post(request: Dict[str, Any], endpoint: str) -> int:
    """Post request to the given endpoint with the given request data.

    Args:
        request (Dict[str, Any]): The request data to be posted.
        endpoint (str): The endpoint for the request.

    Returns:
        int: The ID of the created item.
    """
    async with ClientSession(headers=Config().get_headers()) as session:
        logger.debug(f"debugging for endpoint {endpoint}")
        logger.debug(request)
        if not request['data'].get('firmenlogo', None):
            request['data']['firmenlogo'] = None

        if not request['data'].get('logo', None):
            request['data']['logo'] = None
        logger.debug(request)
        async with session.post(
            url=f"{strapi_url}/{endpoint}", data=json.dumps(request)
        ) as response:
            data = await response.text()
            response_data = json.loads(data)
            logger.debug(response_data)

            if "data" in response_data:
                return response_data["data"]["id"]
            else:
                raise Exception("Error in post request")


async def create_aussteller(aussteller: Dict[str, Any]) -> int:
    """Create an exhibitor with the given data.

    Args:
        aussteller (Dict[str, Any]): The exhibitor data.

    Returns:
        int: The ID of the created exhibitor.
    """
    logger.info("creating Aussteller")
    data = set_aussteller_data(aussteller)
    o = aussteller["onlinemedien"]
    A = MESSESTÄNDE.A.value
    B = MESSESTÄNDE.B.value
    messestand = A if o["messestand"] == ["Messestand A"] else B
    studiengaenge = data["studiengaenge"]
    exhibitor = await Aussteller(
        ausstellerid=aussteller["ausstellerid"],
        messestand=messestand,
        firma=aussteller["firma"],
        land=aussteller["land"],
        ort=aussteller["ort"],
        plz=aussteller["plz"],
        adresszusatz=aussteller["adresszusatz"],
        strasse=aussteller["strasse"],
        homepage=aussteller["homepage"],
        tel=aussteller["telefon"],
        studiengänge=studiengaenge,
        logo_name=o.get("logo_name", None),
        logo=o["logo"],
        firmenlogoName=o.get("firmenlogo_name", None),
        flogo=o.get("logo_onlinemedien", None),
    )

    id = await post(exhibitor.body, "ausstellers")
    exhibitor.set_id(id)
    logger.info(f"created Aussteller {id}")
    return exhibitor


async def create_stellenausschreibungen(
    strapi_aussteller_id, aussteller, logo: int|None, firmenlogo: int|None
) -> None:
    """
    Creates stellenausschreibungen for an aussteller and saves them to a Strapi Aussteller.

    Args:
        strapi_aussteller_id (int): The ID of the aussteller in the Strapi instance.
        aussteller (dict): The aussteller data.
        logo (id|None): The logo file id.
        firmenlogo (int|None): The firmenlogo file id.

    Returns:
        None
    """

    # Create a session with headers from Config
    async with ClientSession(headers=Config().get_headers()) as session:

        # Get stellenausschreibungen data from aussteller
        stellenausschreibungen = aussteller["stellenausschreibungen"]

        # If there are no stellenausschreibungen, return
        if len(stellenausschreibungen) == 0:
            return

        # Prepare files for upload
        files_info = [
            {"file_name": s["file_name"], "file": s["file"]}
            for s in stellenausschreibungen
        ]
        files = upload_multiple_media(files_info)

        # Loop through stellenausschreibungen and create each one
        for i, stelle in enumerate(stellenausschreibungen):

            # Create stellenausschreibung
            stellenausschreibung = await Stellenausschreibung(
                stelle,
                strapi_aussteller_id,
                logo,
                firmenlogo,
                files[i],
                [
                    e
                    for e in [
                        get_studiengänge(stud)
                        for stud in stelle["fakultaet"].split(",")
                    ]
                    if e
                ]
                if (len(stelle["fakultaet"]) != 0)
                else [],
            )

            # Send POST request to create stellenausschreibung in Strapi instance
            async with session.post(
                url=f"{strapi_url}/stellenausschreibungen",
                data=json.dumps(stellenausschreibung.body),
            ) as response:

                # Get response data
                data = await response.text()
                response_id = json.loads(data)["data"]["id"]

                # Log success
                logger.info(
                    f"created Stellenauschreibung {response_id} for Aussteller {strapi_aussteller_id}"
                )


async def create_onlinemedien(strapi_a_id: int, aussteller) -> int:
    """
    Creates onlinemedien for an aussteller and saves them to a Strapi Aussteller.

    Args:
        strapi_a_id (int): The ID of the aussteller in the Strapi instance.
        aussteller (dict): The aussteller data.

    Returns:
        int: The ID of the created onlinemedien.
    """

    # Prepare onlinemedien data
    data = set_onlinemedien_data(aussteller)

    # Create onlinemedien
    onlinemedien = await Onlinemedien(
        strapi_a_id,
        farbcode=data["farbcode"],
        zoom=data["zoom"],
        text=data["text"],
        urls=data["urls"],
        socialmedien=data["socialmedien"],
        unternehmensbeschreibung=data["unternehmensbeschreibung"],
    )
    # Set onlinemedien files
    await onlinemedien.set_onlinemedien_files(aussteller["onlinemedien"])
    id = await post(onlinemedien.body, "onlinemediens")
    logger.info(f"created Onlinemedien {id} for Aussteller {strapi_a_id}")
    return id


async def create_ansprechpartner(strapi_a_id, aussteller) -> int:
    """
    create_ansprechpartner(strapi_a_id, aussteller) -> int

    Creates an Ansprechpartner object for a given Aussteller and posts it to the Strapi API.

    Args:
    strapi_a_id (int): The ID of the Aussteller in the Strapi API.
    aussteller (dict): The dictionary representing the Aussteller to create the Ansprechpartner for.

    Returns:
    int: The ID of the created Ansprechpartner in the Strapi API.

    """
    data = set_ansprechpartner_data(aussteller)
    ansp = await Ansprechpartner(
        strapi_a_id,
        data["name"],
        data["vorname"],
        data["titel"],
        data["anrede"],
        data["email"],
        data["tel"],
    )
    await ansp.set_bild(data["bildName"], data["bild"])
    id = await post(ansp.body, "ansprechpartners")
    logger.info(f"created Ansprechpartner {id} for Aussteller {strapi_a_id}")
    return id
