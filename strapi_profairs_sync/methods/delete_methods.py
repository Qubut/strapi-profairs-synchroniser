"""
This module provides a function to delete an exhibitor's old logos, online media, contact persons, job advertisements, and exhibitor entry from Strapi.

Functions:

    delete(aussteller): 
    Delete an exhibitor's old logos, online media, contact persons, job advertisements, and exhibitor entry from Strapi.
    
    get_id(obj, key): 
    Get the ID of a specific object.
"""


import asyncio
from aiohttp import ClientSession
from logger import logger
from config.endpoints import strapi_url
from classes.aussteller import Aussteller
from classes.onlinemedien import Onlinemedien
from classes.ansprechpartner import Ansprechpartner
from utils.utility import delete_stellen_anzeige


async def delete(aussteller):
    """
    Delete an exhibitor's old logos, online media, contact persons, job advertisements, and exhibitor entry from Strapi.
    
    Args:
    aussteller (dict): The exhibitor data in a dictionary format.

    Returns:
    None
    """
    
    async with ClientSession() as session:
        a = await Aussteller(logo_id=get_id(aussteller,'logo'),firmenlogo_id=get_id(aussteller,'firmenlogo'))
        firma = aussteller['firma']
        await a.delete_old_logos(session)
        logger.info(f'Deleted old logos of {firma}')
        
        o = aussteller['onlinemedien']
        if o:
            onlinemedien = await Onlinemedien(unternehmensprofil=get_id(o,'unternehmensprofil'),bilder=get_id(o,'bilder') or [],flyer=get_id(o,'flyer'))
            await onlinemedien.delete_onlinemedien_files(session)
            await session.delete(url=f"{strapi_url}/onlinemediens/{o['id']}")
            logger.info(f'Deleted onlinemedien of {firma}')
        
        ansp = aussteller['ansprechpartner']
        if ansp:
            ansprechpartner = await Ansprechpartner(bild_id=get_id(ansp,'bild'))
            await ansprechpartner.delete_bild(session)
            await session.delete(url=f"{strapi_url}/ansprechpartners/{ansp['id']}")
            logger.info(f'Deleted Ansprechpartner of {firma}')
        
        stellen_anzeigen_ids = (s["id"] for s in aussteller["stellenausschreibungen"])
        stellen_anzeigen_file_ids = []
        for s in aussteller["stellenausschreibungen"]:
            if f := s.get("file", None):
                if id := f.get("id", None):
                    stellen_anzeigen_file_ids.append(id)
        tasks = [delete_stellen_anzeige(id, file=False) for id in stellen_anzeigen_ids]
        tasks_files = [delete_stellen_anzeige(id) for id in stellen_anzeigen_file_ids]
        await asyncio.gather(*tasks)
        await asyncio.gather(*tasks_files)
        logger.info(f'Deleted Stellenausschreibungen of {firma}')
        
        async with ClientSession() as session:
            await session.delete(url=f"{strapi_url}/ausstellers/{aussteller['id']}")
        logger.info(f'Deleted {firma} {aussteller["id"]}')


def get_id(obj, key):
    """
    Get the ID of a specific object.

    Args:
    obj (dict): The object data in a dictionary format.
    key (str): The key to retrieve the ID from the object.

    Returns:
    str or None: The ID of the object if it exists, or None if it doesn't.
    """
    if obj and key and (result:=obj.get(key,None)):
        if isinstance(result,list):
            return [r['id'] for r in result] if len(result) else []
        return result['id']
    else:
        return None
