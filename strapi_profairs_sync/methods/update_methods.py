"""
Module for updating exhibitor data in Strapi.

This module defines the functions for updating exhibitor data in Strapi based on new data. It includes functions for updating an exhibitor's basic data, online media, and contact person.

The module uses aiohttp library for sending PUT requests to the Strapi API to update the data.

Functions:

    put_request(url: str, data: dict) -> dict:
    Sends a PUT request with the given data to the specified URL using the aiohttp library.

    update_aussteller(session: aiohttp.ClientSession, strapi_a: dict, a: dict, publish: bool) -> Aussteller:
    Updates an Aussteller in Strapi based on new data.

    update_onlinemedien(strapi_a: dict, a: dict, session: aiohttp.ClientSession) -> None:
    Update the Onlinemedien for an Aussteller in Strapi based on new data.

    update_ansprechpartner(strapi_a: dict, a: dict, session: aiohttp.ClientSession) -> None:
    Update an exhibitor's contact person in Strapi.
    
"""

from typing import Any, Dict
import aiohttp
from aiohttp import ClientSession
from utils.utility import (
    delete_stellen_anzeige,
    set_aussteller_data,
    set_onlinemedien_data,
    set_ansprechpartner_data,
    extract_id,
)
from config.endpoints import strapi_url
from config.config import MESSESTÄNDE
from classes.aussteller import Aussteller
from classes.ansprechpartner import Ansprechpartner
from classes.onlinemedien import Onlinemedien
from config.endpoints import strapi_url
from config.config import Config
import rapidjson as json  # use rapidjson instead of json
import asyncio
from logger import logger

headers = Config().get_headers()


async def put_request(url: str, data: Dict[str, Any]):
    """Sends a PUT request with the given data to the specified URL using the aiohttp library.

    Args:
        url (str): The URL to send the request to.
        data (dict): The data to send in the request.

    Returns:
        dict: The response JSON as a dictionary.
    """
    retries = 3  # number of retries before giving up
    retry_delay = 1  # delay between retries in seconds
    while retries > 0:
        try:
            async with aiohttp.ClientSession(headers=headers) as session:
                async with session.put(url, data=json.dumps(data)) as response:
                    return await response.json()
        except:
            retries -= 1
            if retries == 0:
                raise  # if we've run out of retries, re-raise the exception
            else:
                await asyncio.sleep(retry_delay)  # wait before retrying
                continue


async def update_aussteller(session: ClientSession, strapi_a, a):
    """
    Update an Aussteller in Strapi based on new data.

    Args:
        session: aiohttp ClientSession object
        strapi_a: dict representing the existing Aussteller in Strapi
        a: dict representing the new data for the Aussteller

    Returns:
        an Aussteller object representing the updated Aussteller
    """
    data = set_aussteller_data(a)
    A = MESSESTÄNDE.A.value
    B = MESSESTÄNDE.B.value
    messestand = A if a["onlinemedien"]["messestand"] == [
        "Messestand A"] else B
    studiengaenge = data["studiengaenge"]
    aussteller = await Aussteller(
        ausstellerid=a["ausstellerid"],
        messestand=messestand,
        firma=a["firma"],
        land=a["land"],
        ort=a["ort"],
        plz=a["plz"],
        adresszusatz=a["adresszusatz"],
        strasse=a["strasse"],
        homepage=a["homepage"],
        tel=a["telefon"],
        studiengänge=studiengaenge,
        logo_id=extract_id(strapi_a.get("logo")),
        firmenlogo_id=extract_id(strapi_a.get("firmenlogo")),
        id=strapi_a["id"]
    )
    o = a["onlinemedien"]
    await aussteller.delete_old_logos(session)
    await aussteller.set_logos(
        o["firmenlogo_name"], o["firmenlogo"], o["logo_name"], o["logo"]
    )
    url = f"{strapi_url}/ausstellers/{aussteller.id()}"
    await put_request(url, aussteller.body)
    stellen_anzeigen_ids = (s["id"]
                            for s in strapi_a["stellenausschreibungen"])
    stellen_anzeigen_file_ids = []
    for s in strapi_a["stellenausschreibungen"]:
        if f := s.get("file", None):
            if id := f.get("id", None):
                stellen_anzeigen_file_ids.append(id)
    tasks = [delete_stellen_anzeige(id, file=False)
             for id in stellen_anzeigen_ids]
    tasks_files = [delete_stellen_anzeige(id)
                   for id in stellen_anzeigen_file_ids]
    await asyncio.gather(asyncio.gather(*tasks),asyncio.gather(*tasks_files))
    logger.info(f"updated Aussteller {strapi_a['id']}")
    return aussteller


async def update_onlinemedien(strapi_a, a, session: ClientSession):
    """
    Update the Onlinemedien for an Aussteller in Strapi based on new data.

    Args:
        strapi_a: dict representing the existing Aussteller in Strapi
        a: dict representing the new data for the Aussteller
        session: aiohttp ClientSession object

    Returns:
        None
    """
    strapi_o = strapi_a["onlinemedien"]
    id = strapi_o["id"]
    data = set_onlinemedien_data(a)
    onlinemedien = await Onlinemedien(
        strapi_a["id"],
        data["farbcode"],
        data["text"],
        data["urls"],
        data["socialmedien"],
        data["unternehmensbeschreibung"],
        data["zoom"],
        extract_id(strapi_o["unternehmensprofil"]),
        extract_id(strapi_o["flyer"]),
        [b["id"] for b in strapi_o["bilder"]] if strapi_o["bilder"] else [],
    )
    await onlinemedien.delete_onlinemedien_files(session),
    await onlinemedien.set_onlinemedien_files(a["onlinemedien"])
    url = f"{strapi_url}/onlinemediens/{id}"
    result = await put_request(url, onlinemedien.body)
    logger.info(f"Updated onlinemedin {result['data']['id']}")


async def update_ansprechpartner(strapi_a, a, session: ClientSession):
    """
    Update an exhibitor's contact person in Strapi.

    Args:
    strapi_a (dict): The exhibitor data in Strapi in a dictionary format.
    a (dict): The exhibitor data in a dictionary format.
    session (ClientSession): The aiohttp ClientSession.

    Returns:
    None
    """
    strapi_ansp = strapi_a.get("ansprechpartner")
    if strapi_ansp and (id := strapi_ansp.get("id")):
        data = set_ansprechpartner_data(a)
        ansprechpartner = await Ansprechpartner(
            strapi_a["id"],
            data["name"],
            data["vorname"],
            data["titel"],
            data["anrede"],
            data["email"],
            data["tel"],
            bild_id=extract_id(strapi_ansp.get("bild")),
        )
        await asyncio.gather(ansprechpartner.delete_bild(session),
                             ansprechpartner.set_bild(data.get("bildName"), data.get("bild")))
        url = f"{strapi_url}/ansprechpartners/{id}"
        result = await put_request(url, ansprechpartner.body)
        logger.info(f"Updated Ansprechpartner {result['data']['id']}")
