from functools import lru_cache
import json
import re
from typing import Dict, List, Any
from config.config import Config
from config.endpoints import strapi_url, endpoint_files, profairs_url
import asyncio
from aiohttp import ClientSession
from requests import get
import aiohttp
from logger import logger

config = Config()


async def get_strapi_ausstellers(published=True) -> list:
    if published:
        url = f"{strapi_url}/ausstellers?populate[onlinemedien][populate]=*&populate[ansprechpartner][populate]=*&populate[stellenausschreibungen][populate]=*&pagination[pageSize]=10000&populate[logo][populate]=*&populate[firmenlogo][populate]=*"
    else:
        url = f"{strapi_url}/ausstellers?publicationState=preview&pagination[pageSize]=10000&filters[publishedAt][$null]=true"
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                data = await response.json()
                logger.info(
                f"Strapi {'published' if published else 'unpublished'} Ausstellers retrieved")
                return data["data"]
    except Exception as e:
        logger.critical("Connection Error")
        logger.critical(f"An error occurred while getting ProfairsData: {str(e)}")
        exit(1)

async def get_profairs_data():
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(profairs_url) as response:
                profairsData = await response.json()
                logger.info("ProfairsData gotten")
                return profairsData

    except Exception as e:
        logger.critical("Connection Error")
        logger.critical(f"An error occurred while getting ProfairsData: {str(e)}")
        exit(1)    

def set_aussteller_data(a) -> Dict[str, List[int]]:
    p = a["printmedien"]
    keys = p.keys()
    studiengänge: List[str] = []
    for f in config.get_fakultäten():
        if f in keys:
            studiengänge += re.split(r"\,(?![^\(\)]*?\))", p[f])
    return {
        "studiengaenge": [
            s
            for s in set(
                filter(
                    None, [get_studiengänge(std)
                           for std in studiengänge if len(std)]
                )
            )
        ]
    }


def set_onlinemedien_data(a):
    o = a["onlinemedien"]
    farbcode: str = o["farbcode"]
    text: str = (r"\n").join(
        str(o["text"]).strip().replace('"', r"\"").splitlines()
    ) or ""
    unternehmensbeschreibung: str = f"""{o["unternehmensbeschreibung"]}"""
    videoUrls = json.dumps([o["videourl"]]) or {"videoUrls": []}
    socialmedien = (
        json.dumps([o["socialmedia_1"].strip(),
                   o["socialmedia_2"].strip()]) or []
    )
    zoom = json.dumps([o["zoom_1"], o["zoom_2"]]) or []
    return {
        "farbcode": farbcode,
        "text": text,
        "unternehmensbeschreibung": unternehmensbeschreibung,
        "urls": videoUrls,
        "socialmedien": socialmedien,
        "zoom": zoom,
    }


def set_ansprechpartner_data(aussteller):
    a = aussteller["onlinemedien"]["ansprechpartner"]
    tel = a.get("telefon", "")
    name = a.get("name", "")
    vorname = a.get("vorname", "")
    titel = a.get("titel", "")
    bild = a.get("bild", "")
    bildName = a.get("bild_name", "")
    anrede = a.get("anrede", "")
    email = a.get("email", "")
    return {
        "tel": tel,
        "name": name,
        "vorname": vorname,
        "titel": titel,
        "bild": bild,
        "bildName": bildName,
        "anrede": anrede,
        "email": email,
    }


async def delete_stellenausschreibungen_files(aussteller, session: ClientSession):
    """
    Deletes all files associated with each stellenausschreibungen object of an aussteller.

    Args:
        aussteller (dict): The aussteller object whose stellenausschreibungen files need to be deleted.

    Returns:
        None
    """
    data = aussteller["stellenausschreibungen"]
    tasks = []
    for s in data:
        if s.get("logo", None):
            tasks.append(
                asyncio.create_task(
                    session.delete(
                        f"{strapi_url}{endpoint_files}{s['logo']['id']}")
                )
            )
        if s.get("file", None):
            tasks.append(
                asyncio.create_task(
                    session.delete(
                        f"{strapi_url}{endpoint_files}{s['file']['id']}")
                )
            )
        tasks.append(
            asyncio.create_task(
                session.delete(
                    f"{strapi_url}/stellenausschreibungen/{s['id']}")
            )
        )
    await asyncio.gather(*tasks)


@lru_cache(maxsize=None)
def get_studiengänge(stud: str):
    studiengänge = {
        s["studiengang"]: s["id"]
        for s in json.loads(
            get(f"{strapi_url}/studiengangs?pagination[limit]=100").text
        )["data"]
    }
    stud = stud.strip()
    if stud in studiengänge.keys():
        return studiengänge[stud]
    return None


async def delete_stellen_anzeige(id, file=True):
    if file:
        url = f"{strapi_url}{endpoint_files}{id}"
    else:
        url = f"{strapi_url}/stellenausschreibungen/{id}"
    async with ClientSession() as session:
        async with session.delete(url) as response:
            logger.info(f"Deleted {id}: {response.status}")
            return response.status


def extract_id(obj: Dict[str, Any]) -> int | None:
    if obj and (id := obj.get("id")):
        return id
    return None
