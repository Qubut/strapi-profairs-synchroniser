import asyncio
from multiprocessing import Pool, cpu_count
import re
from aiohttp import ClientSession, FormData
from typing import Dict, List
from config.endpoints import strapi_url, endpoint_files_upload
from config.config import Config

from logger import logger
import re
import urllib.request
import requests
import rapidjson as json


def upload_single_media(file):
    """
    Uploads a single media file to Strapi using the Strapi Media Library API.

    :param file: a dictionary containing information about the file to be uploaded.
           The dictionary should have two keys: 'file_name' and 'file'.
           'file_name' is a string representing the name of the file.
           'file' is a string representing the URL of the file.
    :return: the ID of the uploaded file, if the upload was successful. Otherwise, returns None.
    """

    file_name = re.sub("[\?]", "", file["file_name"])
    file_link = file["file"]
    if file_link != "":
        f = urllib.request.urlopen("http:" + file_link + "?")
        if file_name.endswith(".pdf"):
            files = {"files": (file_name, f, "application/pdf", {"uri": ""})}
        else:
            files = {"files": (file_name, f, "image", {"uri": ""})}
        response = requests.post(url=strapi_url + endpoint_files_upload, files=files)
        f.close()
        responseJson = response.json()
        if type(responseJson) == list:
            logger.info(f"Uploaded file '{file_name}' with ID '{responseJson[0]['id']}'.")
            id = responseJson[0]["id"]
            return id
        else:
            return None
    else:
        return None

def upload_multiple_media(files):
    """
    Uploads multiple media files to Strapi using the Strapi Media Library API.

    :param files: a list of dictionaries, where each dictionary contains information about a file to be uploaded.
           Each dictionary should have two keys: 'file_name' and 'file'.
           'file_name' is a string representing the name of the file.
           'file' is a string representing the URL of the file.
    :return: a list of IDs corresponding to the uploaded files, in the same order as the input list.
    """

    with Pool(cpu_count()) as pool:
        ids = pool.map(upload_single_media, files)
    return ids