from typing import List
from datetime import datetime
from .body_holder import BodyHolder
from .aobject import aobject
from config.endpoints import strapi_url, endpoint_files
from aiohttp import ClientSession
from utils.upload_media import upload_single_media
from logger import logger


class Aussteller(BodyHolder, aobject):
    async def __init__(
        self,
        ausstellerid: int = 0,
        messestand: int = 0,
        firma: str = '',
        land: str = '',
        ort: str = '',
        plz: int = 0,
        adresszusatz: str = '',
        strasse: str = '',
        homepage: str = '',
        tel: str = '',
        studiengänge: List[int] = [],
        logo_name: str = None,
        logo: str = None,
        firmenlogoName: str = None,
        flogo: str = None,
        logo_id: int = None,
        firmenlogo_id: int = None,
        id: int = None,
    ) -> None:
        """
        Initializes an exhibitor instance with the given properties.

        Args:
            ausstellerid (int): The exhibitor's ID.
            messestand (int): The exhibitor's booth number.
            firma (str): The exhibitor's company name.
            land (str): The exhibitor's country.
            ort (str): The exhibitor's city.
            plz (int): The exhibitor's postal code.
            adresszusatz (str): Additional address information.
            strasse (str): The exhibitor's street name and number.
            homepage (str): The exhibitor's website URL.
            tel (str): The exhibitor's telephone number.
            studiengänge (List[int]): A list of the IDs of the study programs the exhibitor is interested in.
            logo_name (str): Name of the logo(s) to be uploaded.
            logo (str): Base64-encoded string representation of the logo(s) to be uploaded.
            firmenlogoName (str): Name of the firmenlogo to be uploaded.
            flogo (str): Base64-encoded string representation of the firmenlogo to be uploaded.
            logo_id (int): The ID of the exhibitor's logo image.
            firmenlogo_id (int): The ID of the exhibitor's company logo image.
            id (int): The ID of the exhibitor in the DB.
            publish (bool): determines if Exhibitor appears through the API

        """
        BodyHolder.__init__(self)
        await aobject.__init__(self)
        self._logo = logo_id
        self._firmenlogo = firmenlogo_id
        self._body = {
            "data": {
                "ausstellerid": ausstellerid,
                "land": land,
                "logo": logo,
                "firmenlogo": flogo,
                "firma": firma,
                "ort": ort,
                "adresszusatz": adresszusatz,
                "strasse": strasse,
                "plz": plz,
                "telefon": tel,
                "homepage": homepage,
                "aktualisieren": 0,
                "studiengangs": studiengänge,
                "messestand": messestand,
            }
        }
        self._body["data"]["publishedAt"] = (
            str(datetime.now()).replace(" ", "T") + "Z"
        )
        self._id = id

        if logo_id is not None:
            self._body["data"]["logo"] = logo_id
            self._logo = logo_id
        elif logo_name is not (None or "") and logo is not (None or ""):
            await self.set_logos(logo_name=logo_name, logo=logo)

        if firmenlogo_id is not None:
            self._body["data"]["firmenlogo"] = firmenlogo_id
            self._firmenlogo = firmenlogo_id
        elif firmenlogoName is not (None or "") and flogo is not (None or ""):
            await self.set_logos(firmenlogoName=firmenlogoName, flogo=flogo)

    @property
    def firmenlogo(self):
        return self._firmenlogo

    @property
    def logo(self):
        return self._logo

    def id(self) -> int:
        """
        Returns the ID of the exhibitor.

        Raises:
            ValueError: If the ID has not been set yet.
        """
        if self._id is None:
            raise ValueError("ID not set")
        return self._id

    def set_id(self, id: int) -> None:
        """
        Sets the ID of the exhibitor.

        Args:
            id (int): The ID to set.
        """
        self._id = id

    async def set_logos(
        self,
        firmenlogoName: str = None,
        flogo: str = None,
        logo_name: str = None,
        logo: str = None,
    ) -> None:
        """
        Uploads the exhibitor's firmenlogo and logos.

        Args:
            firmenlogoName (str): Name of the firmenlogo.
            flogo (str): Base64-encoded string representation of the firmenlogo.
            logo_name (str): Name of the logo(s).
            logo (str): Base64-encoded string representation of the logo(s).

        Returns:
            None

        Raises:
            None
        """
        flogo = (
            upload_single_media({"file_name": firmenlogoName, "file": flogo})
            if flogo and firmenlogoName
            else None
        )
        logo = (
            upload_single_media({"file_name": logo_name, "file": logo})
            if logo and logo_name
            else None
        )
        self._firmenlogo = flogo
        self._logo = logo
        self._body["data"]["firmenlogo"] = flogo
        self._body["data"]["logo"] = logo

    async def delete_old_logos(self, session: ClientSession):
        """
        Deletes old logos of the exhibitor.

        Args:
            session (aiohttp.ClientSession): The HTTP client session to use for making requests.

        Raises:
            Exception: If any of the logo deletion requests fail.
        """
        if self._logo:
            async with session.delete(
                f"{strapi_url}{endpoint_files}{self._logo}"
            ) as response:
                if response.status != 200:
                    logger.error(f"Failed to delete logo with ID {self._logo}")
        if self._firmenlogo:
            async with session.delete(
                f"{strapi_url}{endpoint_files}{self._firmenlogo}"
            ) as response:
                if response.status != 200:
                    logger.error(
                        f"Failed to delete firmenlogo with ID {self._firmenlogo}"
                    )
