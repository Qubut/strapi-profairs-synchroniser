import json
from typing import List, Dict, Any
from datetime import datetime
from .body_holder import BodyHolder
from config.endpoints import strapi_url, endpoint_files
from aiohttp import ClientSession
from utils.upload_media import upload_single_media, upload_multiple_media
from .aobject import aobject
from logger import logger


class Onlinemedien(BodyHolder, aobject):
    """
    Represents online media for an exhibitor.

    Attributes:
        ausstellerid (int): ID of the exhibitor.
        farbcode (str): Color code for the media.
        bilder (List[int]): List of image IDs.
        text (str): Media text.
        urls (List[str]): List of video URLs.
        socialmedien (List[str]): List of social media URLs.
        unternehmensbeschreibung (str): Company description.
        unternehmensprofil (int): ID of the company profile.
        flyer (int): ID of the flyer.
        zoom (List[str]): List of zoom URLs.
    """

    async def __init__(
        self,
        ausstellerid: int=0,
        farbcode: str='',
        text: str='',
        urls: List[str]=[],
        socialmedien: List[str]=[],
        unternehmensbeschreibung: str='',
        zoom: List[str] = [],
        unternehmensprofil: int = None,
        flyer: int = None,
        bilder: List[int] = [],
    ) -> None:
        """
        Initializes an instance of the Onlinemedien class.

        Args:
            ausstellerid (int): ID of the exhibitor.
            farbcode (str): Color code for the media.
            bilder (List[int]): List of image IDs.
            text (str): Media text.
            urls (List[str]): List of video URLs.
            socialmedien (List[str]): List of social media URLs.
            unternehmensbeschreibung (str): Company description.
            unternehmensprofil (int): ID of the company profile.
            flyer (int): ID of the flyer.
            zoom (List[str]): List of zoom URLs.
        """
        await super().__init__()
        self._unternehmensprofil = unternehmensprofil or None
        self._flyer = flyer or None
        self._bilder = [b for b in bilder if b] or []
        self._body = {
            "data": {
                "publishedAt": str(datetime.now()).replace(" ", "T") + "Z",
                "farbcode": farbcode.strip() or "",
                "unternehmensprofil": self._unternehmensprofil,
                "unternehmensbeschreibung": unternehmensbeschreibung or "",
                "flyer": self._flyer,
                "videoUrls": urls,
                "socialmedien": json.loads(socialmedien) if len(socialmedien) else None,
                "zoom": json.loads(zoom) if len(zoom) else None,
                "bilder": self._bilder,
                "text": text or None,
                "aussteller": ausstellerid,
            }
        }

    async def delete_onlinemedien_files(self, session: ClientSession):
        """
        This async function deletes the online media files associated with an exhibitor,
        using aiohttp for HTTP requests.

        Args:
        None

        Returns:
        None

        Raises:
        None"""
        if self._unternehmensprofil:
            async with session.delete(
                f"{strapi_url}{endpoint_files}{self._unternehmensprofil}"
            ) as response:
                if response.status != 200:
                    logger.erorr(
                        f"Failed to delete unternehmensprofil with ID {self._unternehmensprofil}"
                    )
        if self._flyer:
            async with session.delete(
                f"{strapi_url}{endpoint_files}{self._flyer}"
            ) as response:
                if response.status != 200:
                    logger.error(f"Failed to delete flyer with ID {self._flyer}")
        if self._bilder:
            for bild in self._bilder:
                async with session.delete(
                    f"{strapi_url}{endpoint_files}{bild}"
                ) as response:
                    if response.status != 200:
                        logger.error(f"Failed to delete bild with ID {bild}")

    async def set_onlinemedien_files(self, o: Dict[str, Any]):
        """
        This async function uploads online media files associated with an exhibitor,
        using aiohttp for HTTP requests.

        Returns:
        Dictionary containing the IDs of the uploaded media files.
        The keys are 'bilder', 'unternehmensprofil', and 'flyer'.
        The 'flyer' key may have a value of None if no flyer was provided.
        """
        bilder: List[int] = upload_multiple_media(o["bilder"])
        firmenflyerName = o.get("firmenflyer_name", "")
        firmenflyer = o.get("firmenflyer", "")
        unternehmensprofil: int = upload_single_media(
            {"file_name": o["file_name"], "file": o["file"]}
        )
        flyer: int = (
            upload_single_media(
                {"file_name": firmenflyerName, "file": firmenflyer}
            )
            if firmenflyer != ""
            else None
        )
        self._flyer = flyer
        self._unternehmensprofil = unternehmensprofil
        self._bilder = [b for b in bilder if b]
        self._body["data"]["flyer"] = flyer
        self._body["data"]["unternehmensprofil"] = unternehmensprofil
        self._body["data"]["bilder"] = self._bilder
