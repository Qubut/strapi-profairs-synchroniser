from typing import List, Dict
from datetime import datetime
from .body_holder import BodyHolder
from .aobject import aobject


class Stellenausschreibung(BodyHolder, aobject):
    """
    This class represents a job posting.

    Attributes:
    -----------
    _body : Dict[str, Union[str, List, int]]
        The dictionary that holds the details of the job posting.
    """

    async def __init__(
        self,
        s: Dict[str, str],
        ausstellerid: int,
        logo: int,
        firmenlogo: int,
        file:int,
        studiengaenge: List[int],
        backslash=r"\"",
    ):
        """
        Constructor for the Stellenausschreibung class.

        Parameters:
        -----------
        s : Dict[str, str]
            A dictionary that holds the details of the job posting.
        ausstellerid : int
            The ID of the job advertiser.
        logo : int
            The ID of the logo of the job advertiser.
        firmenlogo : int
            The ID of the firm logo of the job advertiser.
        studiengaenge : List[int]
            A list of integers representing the academic programs associated with the job posting.
        backslash : str, optional
            The backslash character to use as an escape character in the job posting title.
            Default is '\"'.
        """
        BodyHolder.__init__(self)
        await aobject.__init__(self)
        self._body = {
            "data": {
                "publishedAt": str(datetime.now()).replace(" ", "T") + "Z",
                "art": s["art"].strip(" ") if len(s["art"]) != 0 else "NA",
                "datum": str(datetime.strptime(s["datum"], "%B, %d %Y %H:%M:%S")).split(
                    " "
                )[0],
                "titel": s["titel"].replace('"', backslash),
                "studiengangs": studiengaenge,
                "schlagworte": [
                    "{0}".format(w)
                    for w in s["schlagworte"].replace(" ", "").split(",")
                ],
                "logo": logo or firmenlogo or None,
                "file": file,
                "aussteller": ausstellerid,
            }
        }

    @property
    def body(self):
        return self._body
