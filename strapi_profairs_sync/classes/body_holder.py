from typing import List, Dict


class BodyHolder:
    """
    This class represents a holder for the common implementation of the `body` property.
    """

    _body: Dict[str, str | List | int] = {}

    @property
    def body(self):
        """
        A getter method that returns the body details.

        Raises:
        -------
        ValueError:
            If the _body attribute is not set.

        Returns:
        -------
        Dict[str, Union[str, List, int]]:
            The dictionary that holds the body details.
        """
        if self._body:
            return self._body
        else:
            raise ValueError("body is not set")
