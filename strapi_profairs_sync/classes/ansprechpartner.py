from typing import List
from config.endpoints import strapi_url, endpoint_files
from aiohttp import ClientSession
from datetime import datetime
from utils.upload_media import upload_single_media
from .body_holder import BodyHolder
from .aobject import aobject
from logger import logger


class Ansprechpartner(BodyHolder, aobject):
    """
    Represents a point of contact for a job advertiser.

    Attributes:
    -----------
    ausstellerid : int
        The ID of the job advertiser.
    name : str
        The last name of the point of contact.
    vorname : str
        The first name of the point of contact.
    titel : str
        The title of the point of contact.
    anrede : str
        The salutation used to address the point of contact.
    email : str
        The email address of the point of contact.
    tel : str
        The phone number of the point of contact.
    bild : List[int]
        A list of integers representing the image associated with the point of contact.
    bildName : str
        The name of the image associated with the point of contact.

    Methods:
    --------
    __init__(
        ausstellerid: int,
        name: str,
        vorname: str,
        titel: str,
        anrede: str,
        email: str,
        tel: str,
        bild: List[int],
        bildName: str,
    )
        Initializes a new instance of the Ansprechpartner class with the specified attributes.
    """

    async def __init__(
        self,
        ausstellerid: int=0,
        name: str='',
        vorname: str='',
        titel: str='',
        anrede: str='',
        email: str='',
        tel: str='',
        bild: List[int] = None,
        bild_name: str = None,
        bild_id: int = None,
    ):
        """
        Initializes a new instance of the Ansprechpartner class with the specified attributes.

        Parameters:
        -----------
        ausstellerid : int
            The ID of the job advertiser.
        name : str
            The last name of the point of contact.
        vorname : str
            The first name of the point of contact.
        titel : str
            The title of the point of contact.
        anrede : str
            The salutation used to address the point of contact.
        email : str
            The email address of the point of contact.
        tel : str
            The phone number of the point of contact.
        bild : List[int]
            A list of integers representing the image associated with the point of contact.
        bildName : str
            The name of the image associated with the point of contact.
        """
        await super().__init__()
        self._bild = bild_id
        self._body = {
            "data": {
                "aussteller": ausstellerid,
                "anrede": anrede,
                "name": name,
                "vorname": vorname,
                "titel": titel if len(titel) else "",
                "email": email if len(email) else "",
                "telefon": tel if len(tel) else "",
                "publishedAt": str(datetime.now()).replace(" ", "T") + "Z",
            }
        }

    async def delete_bild(self, session: ClientSession):
        """
        Deletes the image of the contact person using the Strapi API.

        This function uses the aiohttp library to send an HTTP DELETE request to the Strapi API to delete the
        image file with the ID stored in the instance variable `_bild`. If `_bild` is `None`, this function does
        nothing.

        Raises:
            aiohttp.ClientResponseError: If the API returns a non-204 status code.
        """
        if self._bild:
            async with session.delete(
                f"{strapi_url}{endpoint_files}{self._bild}"
            ) as response:
                if response.status != 200:
                    logger.error(f"Failed to delete bild with ID {self._bild}")

    async def set_bild(self, bild_name: str, bild: str):
        bild_id: int = upload_single_media({"file_name": bild_name, "file": bild})
        self._bild = bild_id
        self._body["data"]["bild"] = bild_id
