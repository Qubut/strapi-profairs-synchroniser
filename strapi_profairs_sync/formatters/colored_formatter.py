"""
This module contains a custom formatter class for logging that adds color to log messages.

Classes:
    ColoredFormatter: A custom formatter class that adds color to log messages.
"""

import logging
from colored import fg, bg, attr


class ColoredLogRecord(logging.LogRecord):
    """
    Subclass of LogRecord that adds a 'log_color' attribute.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.levelname == "DEBUG":
            self.log_color = fg("white")
        elif self.levelname == "INFO":
            self.log_color = fg("green")
        elif self.levelname == "WARNING":
            self.log_color = fg("yellow")
        elif self.levelname == "ERROR":
            self.log_color = fg("red")
        elif self.levelname == "CRITICAL":
            self.log_color = fg("red") + bg("white") + attr("bold")
        else:
            self.log_color = ""

class ColoredFormatter(logging.Formatter):
    """
    A custom logging formatter that adds color to the log messages based on their severity level.

    Attributes:
        - LOG_FORMAT (str): The default format string for log messages.
    """
    LOG_FORMAT = "%(asctime)s [%(levelname)s] %(message)s"

    def format(self, record):
        """
        Format the log record with color based on its severity level.

        Args:
            - record (logging.LogRecord): The log record to format.

        Returns:
            - str: The formatted log message with color.
        """
        message = super().format(record)
        reset_color = attr("reset")
        return f"{record.log_color}{message}{reset_color}"
