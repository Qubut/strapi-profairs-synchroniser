"""
This module contains a script that updates exhibitor data on a remote API endpoint by retrieving JSON data from another API endpoint,
making updates to the data, and then sending the updated data back to the remote endpoint.

The script uses asyncio and aiohttp to make asynchronous HTTP requests and update the data. It also uses logging to output status messages.

The main function in this module is `main()`, which contains the main program loop. The loop retrieves JSON data from the Strapi API endpoint,
updates the exhibitor data using the data from the Profairs API endpoint, and creates new job listings on the remote endpoint.

To run the script, simply execute the script directly from the command line or import it into another module and call the main function.

"""


import asyncio
from aiohttp import ClientSession

from logger import logger
import os
import time
import config.endpoints as endpoints
from methods.create_methods import create_stellenausschreibungen
from methods.update_methods import (
    update_ansprechpartner,
    update_aussteller,
    update_onlinemedien,
)

os.chdir(os.path.dirname(os.path.abspath(__file__)))

# Endpoint URLs
strapi_url = (
    endpoints.strapi_url + endpoints.endpoint_strapi_25_aussteller_aktualisieren
)
profairs_specific = endpoints.profairs_specific_aussteller


async def get_act_json(session: ClientSession) -> dict:
    """
    Retrieve JSON data from Strapi API endpoint.

    :param session: aiohttp ClientSession object for making HTTP requests
    :return: Dictionary containing JSON data from API endpoint
    """
    async with session.get(strapi_url) as response:
        response.raise_for_status()
        data = await response.json()
        logger.info("CheckData gotten")
        return data["data"]


async def get_profairs_specific_data(session: ClientSession, id: int) -> dict:
    """
    Retrieve specific JSON data from Profairs API endpoint.

    :param session: aiohttp ClientSession object for making HTTP requests
    :param id: ID of specific data to retrieve
    :return: Dictionary containing JSON data from API endpoint
    """
    url = profairs_specific + str(id)
    async with session.get(url) as response:
        response.raise_for_status()
        data = await response.json()
        logger.info(url)
        logger.info(data)
        return data[0]


async def main():
    """
    Main program loop.
    """
    publish = False
    async with ClientSession() as session:
        while True:
            act_json = await get_act_json(session)
            logger.debug(act_json)
            if not act_json:
                time.sleep(1)
            for strapi_a in act_json:
                logger.info("update ausstellers")
                aussteller = await get_profairs_specific_data(
                    session, strapi_a["ausstellerid"]
                )
                updated_a = await update_aussteller(
                    session, strapi_a, aussteller, publish
                )
                await update_ansprechpartner(strapi_a, aussteller, session)
                await update_onlinemedien(strapi_a, aussteller, session)
                await create_stellenausschreibungen(
                    updated_a.id(),
                    aussteller,
                    updated_a.logo,
                    updated_a.firmenlogo,
                )
                break


if __name__ == "__main__":
    logger.info("updateScript was started")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
