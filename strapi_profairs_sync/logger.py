import os
import logging
from datetime import datetime
from colored import attr
from formatters.colored_formatter import ColoredFormatter, ColoredLogRecord

now = datetime.now()
timestamp = now.strftime("%Y-%m-%d_%H-%M-%S")

# Define logger format
LOG_FORMAT = "%(asctime)s [%(levelname)s] %(message)s"
COLORED_LOG_FORMAT = "%(log_color)s%(asctime)s [%(levelname)s] %(message)s" + attr(
    "reset"
)

# Create logger instance
logger = logging.getLogger(__name__)

# Set logging level
logger.setLevel(logging.DEBUG)

# Create logs directory if it doesn't exist
log_dir = "logs"
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

# Create file handler for logging to file
log_file = os.path.join(log_dir, f"poetry_project_{timestamp}.log")
file_handler = logging.FileHandler(filename=log_file, mode="a")
file_handler.setFormatter(logging.Formatter(LOG_FORMAT))

# Create console handler for logging to console
console_handler = logging.StreamHandler()
console_formatter = ColoredFormatter(COLORED_LOG_FORMAT)
console_handler.setFormatter(console_formatter)

# Override LogRecord class to add log_color attribute
logging.setLogRecordFactory(ColoredLogRecord)

# Add handlers to logger
logger.addHandler(file_handler)
logger.addHandler(console_handler)
