# Strapi-Profairs-Sync

Strapi-Profairs-Sync is a Python script that enables synchronization of data between Strapi CMS and the virtual platform of the university career fairs Profairs. The package uses asyncio and aiohttp to enable efficient communication between Strapi and Profairs APIs.

it uses poetry as its dependency management tool.


it consists of two parts:

- `main.py`: should run every midnight to update Strapi_DB
- `specific_update.py`: A manually runned script to update the data of specific exhibitors
 
## Installation

To initialize Strapi-Profairs-Sync, run:

```sh
poetry install
```

# Usage

#### Documentation

to build the documentation run

```sh
cd ./docs && poetry run sphinx-build -b html source build
```

Here's an example of how to use Strapi-Profairs-Sync:

### From the command line:

- `poetry run python strapi_profairs_sync/main.py`
- `poetry run python strapi_profairs_sync/specific_update.py`

### in another project:

```py

from strapi_profairs_sync import main as sync_data

# call sync_data function to start synchronization
sync_data()

```

# Contributing

Contributions are welcome! Please feel free to submit a pull request or open an issue if you find a bug or have a feature request.
License

This project is licensed under the MIT License - see the LICENSE file for details.